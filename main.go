package main

import (
	"context"
	"fmt"

	"26b.uk/go-todo/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const uri = "mongodb://localhost:27017"

func main() {
	// fmt.Println("Not implemented yet")

	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}
	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	// Ping the primary
	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		panic(err)
	}
	fmt.Println("Successfully connected and pinged.")
	collection := client.Database("testing").Collection("todos")

	// res, err := collection.InsertOne(context.TODO(), bson.D{{"name", "pi"}, {"value", 3.14159}})
	// res, err := collection.InsertOne(context.TODO(), models.Todo{"222", "Eating"})
	// fmt.Println("insert", res, err)

	// filter := bson.D{{"id", "111"}}
	// var result *models.Todo
	// err = collection.FindOne(context.TODO(), filter).Decode(&result)
	// fmt.Println("everything", result.Description, err)

	cur, err := collection.Find(context.TODO(), bson.D{})
	if err != nil {
		fmt.Println("query failed", err)
	}
	// defer cur.Close(context.TODO())
	for cur.Next(context.TODO()) {
		var result *models.Todo
		err := cur.Decode(&result)
		if err != nil {
			fmt.Println("decode failed", err)
		}
		fmt.Println(result)
		// do something with result....
	}
	if err := cur.Err(); err != nil {
		fmt.Println("cur failed", err)
	}
	fmt.Println("everything", cur, err)
}
