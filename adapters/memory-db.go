package adapters

import (
	"strconv"
)

type MemoryDB struct {
	id    int
	todos []interface{}
}

func (d *MemoryDB) NextID() string {
	d.id++
	return strconv.Itoa(d.id)
}

func (d *MemoryDB) Add(record interface{}) {
	d.todos = append(d.todos, record)
}

func (d *MemoryDB) GetAll() []interface{} {
	return d.todos
}
