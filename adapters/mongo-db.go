package adapters

import (
	"context"
	"fmt"
	"strconv"

	"26b.uk/go-todo/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoDB struct {
	collection *mongo.Collection
	id         int
}

func NewMongoDB(collection *mongo.Collection) *MongoDB {
	return &MongoDB{collection: collection}
}

func (d *MongoDB) NextID() string {
	d.id++
	return strconv.Itoa(d.id)
}

func (d *MongoDB) Add(record interface{}) {
	res, err := d.collection.InsertOne(context.TODO(), record)
	fmt.Println(err, res)
}

func (d *MongoDB) GetAll() []interface{} {
	var todos []interface{}
	cur, err := d.collection.Find(context.TODO(), bson.D{})
	if err != nil {
		fmt.Println("query failed", err)
	}
	for cur.Next(context.TODO()) {
		var result *models.Todo //need to find way to remove reference to model!!
		err := cur.Decode(&result)
		if err != nil {
			fmt.Println("decode failed", err)
		}
		fmt.Println(result)
		todos = append(todos, *result)
	}
	return todos
}
