package models

type Todo struct {
	ID          string
	Description string
}
