package services

import (
	"26b.uk/go-todo/models"
	"26b.uk/go-todo/ports"
)

type Todos struct {
	db ports.DBInterface
}

func NewTodos(db ports.DBInterface) *Todos {
	return &Todos{db}
}

func (t *Todos) Add(description string) (models.Todo, error) {
	todo := models.Todo{
		ID:          t.db.NextID(),
		Description: description,
	}
	t.db.Add(todo)
	return todo, nil
}

func (t *Todos) GetById(id string) (models.Todo, error) {
	for _, item := range t.db.GetAll() {
		todo := item.(models.Todo)
		if todo.ID == id {
			return todo, nil
		}
	}
	return models.Todo{}, nil
}
