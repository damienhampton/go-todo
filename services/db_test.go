package services_test

import (
	"context"
	"testing"

	"26b.uk/go-todo/adapters"
	"26b.uk/go-todo/services"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const uri = "mongodb://localhost:27017"

func TestDBTodos(t *testing.T) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))

	if err != nil {
		t.Error("Could not connect to database")
	}
	collection := client.Database("testing").Collection("todos")
	mongoDB := adapters.NewMongoDB(collection)

	t.Run("Todos", func(t *testing.T) {
		t.Run("Add", func(t *testing.T) {
			t.Run("should add a new todo", func(t *testing.T) {
				todos := services.NewTodos(mongoDB)
				todo, err := todos.Add("Stroke cat")
				if err != nil {
					t.Error("Expected no error, got", err)
				}
				if todo.ID == "" {
					t.Error("Expected a new todo id, got empty string")
				}
				if todo.Description != "Stroke cat" {
					t.Error("Expected todo text to be Stroke cat, got", todo.Description)
				}
			})
		})
		t.Run("Get", func(t *testing.T) {
			t.Run("should get a todo", func(t *testing.T) {
				todos := services.NewTodos(mongoDB)
				resp, _ := todos.Add("Stroke cat")
				todo, err := todos.GetById(resp.ID)

				if err != nil {
					t.Error("Expected no error, got", err)
				}
				if todo.ID == "" {
					t.Error("Expected a new todo id, got empty string")
				}
				if todo.Description != "Stroke cat" {
					t.Error("Expected todo text to be Stroke cat, got", todo.Description)
				}
			})
		})
	})
}
