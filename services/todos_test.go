package services_test

import (
	"testing"

	"26b.uk/go-todo/adapters"
	"26b.uk/go-todo/services"
)

func TestTodos(t *testing.T) {
	memDB := &adapters.MemoryDB{}

	t.Run("Todos", func(t *testing.T) {
		t.Run("Add", func(t *testing.T) {
			t.Run("should add a new todo", func(t *testing.T) {
				todos := services.NewTodos(memDB)
				todo, err := todos.Add("Stroke cat")
				if err != nil {
					t.Error("Expected no error, got", err)
				}
				if todo.ID == "" {
					t.Error("Expected a new todo id, got empty string")
				}
				if todo.Description != "Stroke cat" {
					t.Error("Expected todo text to be Stroke cat, got", todo.Description)
				}
			})
		})
		t.Run("Get", func(t *testing.T) {
			t.Run("should get a todo", func(t *testing.T) {
				todos := services.NewTodos(memDB)
				resp, _ := todos.Add("Stroke cat")
				todo, err := todos.GetById(resp.ID)

				if err != nil {
					t.Error("Expected no error, got", err)
				}
				if todo.ID == "" {
					t.Error("Expected a new todo id, got empty string")
				}
				if todo.Description != "Stroke cat" {
					t.Error("Expected todo text to be Stroke cat, got", todo.Description)
				}
			})
		})
	})
}
