package ports

type DBInterface interface {
	NextID() string
	Add(record interface{})
	GetAll() []interface{}
}
